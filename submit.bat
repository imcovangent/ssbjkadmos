@echo off
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure to upload SSBJ-KADMOS to PyPi (y/n)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

python setup.py sdist upload -r pypi

:END
endlocal

pause