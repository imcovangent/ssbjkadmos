@echo off
setlocal
:PROMPT
SET /P AREYOUSURE=Are you sure to create a new SSBJ-KADMOS distribution (y/n)?
IF /I "%AREYOUSURE%" NEQ "Y" GOTO END

python setup.py sdist
python setup.py bdist_wheel

:END
endlocal

pause